var imageCapture;

function onGetUserMediaButtonClick() {
  navigator.mediaDevices.getUserMedia({video: true})
  .then(mediaStream => {
    document.querySelector('video').srcObject = mediaStream;

    const track = mediaStream.getVideoTracks()[0];
    imageCapture = new ImageCapture(track);
  })
  .catch(error => console.log(error));
}

function onGrabFrameButtonClick() {
  imageCapture.grabFrame()
  .then(imageBitmap => {
    console.log(imageBitmap)
    // const canvas = document.querySelector('#grabFrameCanvas');
    // drawCanvas(canvas, imageBitmap);
  })
  .catch(error => ChromeSamples.log(error));
}

// function onTakePhotoButtonClick() {
//   console.log("inisde on photo click")
//   imageCapture.takePhoto()
//   .then(blob => {console.log(blob)


//   })

//   // .then(blob => createImageBitmap(blob))
//   // .then(imageBitmap => {
//   //   const canvas = document.querySelector('#takePhotoCanvas');
//   //   drawCanvas(canvas, imageBitmap);
//   // })
//   // .then(data => {
//   //   // putimagefiler();
//   // })
//   .catch(error => ChromeSamples.log(error));
// }

/* Utils */

function drawCanvas(canvas, img) {
  // canvas.width = 1280;
  // canvas.height = 720;
   getComputedStyle(canvas).width.split('px')[0];
   getComputedStyle(canvas).height.split('px')[0];
  let ratio  = Math.min(canvas.width / img.width, canvas.height / img.height);
  let x = (canvas.width - img.width * ratio) / 2;
  let y = (canvas.height - img.height * ratio) / 2;
  canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height);
  canvas.getContext('2d').drawImage(img, 0, 0, img.width, img.height,
      x, y, img.width * ratio, img.height * ratio);
}

function putimagefiler(){
    Caman('#takePhotoCanvas', function () {
    this.contrast(15);
    this.exposure(10);
    this.sepia(10);
    this.vignette('10%')
    this.greyscale().render();
  });
  }

// document.querySelector('video').addEventListener('play', function() {
// //   document.querySelector('#grabFrameButton').disabled = false;
//   document.querySelector('#takePhotoButton').disabled = false;
// });