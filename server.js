'use strict';
var nodemailer = require("nodemailer");
var LocalStorage = require('node-localstorage').LocalStorage;
var express = require('express');
var path = require('path');
var port = process.env.PORT || 1337;
var fs = require('fs');
var Jimp = require('jimp');
var app = express();
var bodyParser = require('body-parser');
var crypto = require("crypto");
var date = require('date-and-time')
var url = require('url');
var querystring = require('querystring');
var JsonRecords = require('json-records');
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(express.json());
var captured_image_folder = path.join(__dirname, "captured_images//");
var output_image_folder = path.join(__dirname, "output_images//");
var images_folder = path.join(__dirname, "images//");
var db_folder = path.join(__dirname, "dto//");
var jr = new JsonRecords(path.join(db_folder + 'data.json'));
app.use("/publiccss", express.static(path.join(__dirname, "css")));
app.use("/publicimages", express.static(path.join(__dirname, "images")));
app.use("/publicjs", express.static(path.join(__dirname, "js")));
app.use("/outputimages", express.static(path.join(__dirname, "output_images")));
var localStorage = new LocalStorage('./scratch');
var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'redfortapp2021@gmail.com', pass: 'approutes@123'
    }
});
app.post("/image_post", function (req, res) {
    var parsedUrl = url.parse(req.headers['referer']);
    var parsedQs = querystring.parse(parsedUrl.query);
    var qslen = JSON.stringify(parsedQs);
    var qs_type = 'SwachchBharat_en';
    var qs_lang = 'en';
    if (qslen === '{}') {
    }
    else {
        if (parsedQs.type !== undefined) {
            qs_type = parsedQs.type;
        }
        if (parsedQs.lang !== undefined) {
            qs_lang = parsedQs.lang;
        }
    }
    var base64Data = req.body.iblob.replace(/^data:image\/png;base64,/, "");
    var outfilename = crypto.randomBytes(30).toString('hex') + ".png";
    //console.log("captured image saving path = " + captured_image_folder + outfilename);
    fs.writeFile(captured_image_folder + outfilename, base64Data, 'base64', function (err) {
        if (err) { res.send(""); console.log(err); alert(err); }
        else {
            var bars = images_folder + "//" + qs_lang + "//" + qs_type + ".png";
            Jimp.read(bars).then(tpl => (
                Jimp.read(captured_image_folder + outfilename).then(logoTpl => {
                    logoTpl.cover(640, 1310, Jimp.HORIZONTAL_ALIGN_CENTER | Jimp.VERTICAL_ALIGN_MIDDLE);
                    return tpl.composite(logoTpl, 0, 0, { mode: Jimp.BLEND_DESTINATION_OVER });
                })).then(tpl => (tpl.quality(100).writeAsync(output_image_folder + outfilename))).then(tpl => {
                    localStorage.setItem("pledgetype", qs_type);
                    localStorage.setItem("pledgelang", qs_lang);
                    res.send(outfilename);
                }).catch(err => {
                    console.log(err);
                    res.send("");
                }));
        }
    });
});

app.post("/user_details_post", function (req, res) {
    var now = new Date();
    const value = date.format(now, 'YYYY/MM/DD HH:mm:ss');
    jr.add({
        currentdate: value,
        pledgetype: localStorage.getItem("pledgetype"),
        pledgelang: localStorage.getItem("pledgelang"),
        filename: req.body.caputed_image,
        u_email: req.body.u_email,
        u_tel: req.body.u_tel,
        u_shared: req.body.u_shared
    });
    var fullfilepath = output_image_folder + req.body.caputed_image;
    if (req.body.u_email !== '') {
        var transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: 'redfortApp2021@gmail.com', pass: 'approutes@123'
            }
        });
        transporter.sendMail({
            from: 'redfortApp2021@gmail.com',
            to: req.body.u_email,
            subject: "Red Fort Center Pledge - " + localStorage.getItem("pledgetype"),
            text: "Phone: " + req.body.u_tel,
            attachments: [
                {
                    filename: localStorage.getItem("pledgetype") + ".png",
                    path: fullfilepath
                }
            ]
        }, (err, info) => {
            if (err) {
                console.log(err);
            }
            else {
                console.log('mail sent');
            }
        });
    }
    res.send('1');
});

app.get("/", function (req, res) {
    res.sendFile(__dirname + "/splash.html");
});
app.get("/splash.html", function (req, res) {
    res.sendFile(__dirname + "/splash.html");
});
app.get("/details.html", function (req, res) {
    res.sendFile(__dirname + "/details.html");
});
app.get("/pledgehome.html", function (req, res) {
    res.sendFile(__dirname + "/pledgehome.html");
});
app.get("/snap.html", function (req, res) {
    res.sendFile(__dirname + "/snap.html");
});
//HINDI
app.get("/hindi/splash.html", function (req, res) {
    res.sendFile(__dirname + "/hindi/splash.html");
});
app.get("/hindi/pledgehome.html", function (req, res) {
    res.sendFile(__dirname + "/hindi/pledgehome.html");
});
app.get("/hindi/snap.html", function (req, res) {
    res.sendFile(__dirname + "/hindi/snap.html");
});
app.get("/hindi/details.html", function (req, res) {
    res.sendFile(__dirname + "/hindi/details.html");
});
app.listen(port, () => { console.log(`service is listening to ${port}`) });

